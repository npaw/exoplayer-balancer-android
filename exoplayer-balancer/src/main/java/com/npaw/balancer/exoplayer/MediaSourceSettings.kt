package com.npaw.balancer.exoplayer

import com.google.android.exoplayer2.source.DefaultMediaSourceFactory.AdsLoaderProvider
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ads.AdsLoader
import com.google.android.exoplayer2.ui.AdViewProvider
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy

class MediaSourceSettings private constructor(
    var adsLoaderProviderDeprecated: AdsLoaderProvider? = null,
    var adsLoaderProvider: AdsLoader.Provider? = null,
    val adViewProvider: AdViewProvider? = null,
    val loadErrorHandlingPolicy: LoadErrorHandlingPolicy? = null,
    var drmSessionManagerProvider: com.google.android.exoplayer2.drm.DrmSessionManagerProvider? = null,
    val liveTargetOffsetMs: Long? = null,
    val liveMinOffsetMs: Long? = null,
    val liveMaxOffsetMs: Long? = null,
    val liveMinSpeed: Float? = null,
    val liveMaxSpeed: Float? = null,
    val useProgressiveMediaSourceForSubtitles: Boolean? = null,
    var serverSideAdInsertionMediaSourceFactory: MediaSource.Factory? = null) {

    data class Builder(
        var adsLoaderProviderDeprecated: AdsLoaderProvider? = null,
        var adsLoaderProvider: AdsLoader.Provider? = null,
        var adViewProvider: AdViewProvider? = null,
        var loadErrorHandlingPolicy: LoadErrorHandlingPolicy? = null,
        var drmSessionManagerProvider: com.google.android.exoplayer2.drm.DrmSessionManagerProvider? = null,
        var liveTargetOffsetMs: Long? = null,
        var liveMinOffsetMs: Long? = null,
        var liveMaxOffsetMs: Long? = null,
        var liveMinSpeed: Float? = null,
        var liveMaxSpeed: Float? = null,
        var useProgressiveMediaSourceForSubtitles: Boolean? = null,
        var serverSideAdInsertionMediaSourceFactory: MediaSource.Factory? = null) {

        fun adsLoaderProviderDeprecated(adsLoaderProviderDeprecated: AdsLoaderProvider?) = apply { this.adsLoaderProviderDeprecated = adsLoaderProviderDeprecated }
        fun adsLoaderProvider(adsLoaderProvider: AdsLoader.Provider?) = apply { this.adsLoaderProvider = adsLoaderProvider }
        fun adViewProvider(adViewProvider: AdViewProvider?) = apply { this.adViewProvider = adViewProvider }
        fun loadErrorHandlingPolicy(loadErrorHandlingPolicy: LoadErrorHandlingPolicy?) = apply { this.loadErrorHandlingPolicy = loadErrorHandlingPolicy }
        fun drmSessionManagerProvider(drmSessionManagerProvider: com.google.android.exoplayer2.drm.DrmSessionManagerProvider?) = apply { this.drmSessionManagerProvider = drmSessionManagerProvider }
        fun liveTargetOffsetMs(liveTargetOffsetMs: Long) = apply { this.liveTargetOffsetMs = liveTargetOffsetMs }
        fun liveMinOffsetMs(liveMinOffsetMs: Long) = apply { this.liveMinOffsetMs = liveMinOffsetMs }
        fun liveMaxOffsetMs(liveMaxOffsetMs: Long) = apply { this.liveMaxOffsetMs = liveMaxOffsetMs }
        fun liveMinSpeed(liveMinSpeed: Float) = apply { this.liveMinSpeed = liveMinSpeed }
        fun liveMaxSpeed(liveMaxSpeed: Float) = apply { this.liveMaxSpeed = liveMaxSpeed }
        fun experimentalUseProgressiveMediaSourceForSubtitles(useProgressiveMediaSourceForSubtitles: Boolean) = apply { this.useProgressiveMediaSourceForSubtitles = useProgressiveMediaSourceForSubtitles }
        fun serverSideAdInsertionMediaSourceFactory(serverSideAdInsertionMediaSourceFactory: MediaSource.Factory?) = apply { this.serverSideAdInsertionMediaSourceFactory = serverSideAdInsertionMediaSourceFactory }

        fun build() = MediaSourceSettings(adsLoaderProviderDeprecated, adsLoaderProvider, adViewProvider, loadErrorHandlingPolicy, drmSessionManagerProvider, liveTargetOffsetMs, liveMinOffsetMs, liveMaxOffsetMs, liveMinSpeed, liveMaxSpeed, useProgressiveMediaSourceForSubtitles, serverSideAdInsertionMediaSourceFactory)
    }
}