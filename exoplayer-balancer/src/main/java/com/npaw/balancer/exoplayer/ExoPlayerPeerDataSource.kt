package com.npaw.balancer.exoplayer

import android.net.Uri
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.upstream.BaseDataSource
import com.google.android.exoplayer2.upstream.ByteArrayDataSink
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.npaw.balancer.loaders.data.DataManager
import com.npaw.balancer.loaders.data.PeerManager
import com.npaw.balancer.models.data.DataSourceId
import com.npaw.balancer.stats.events.DownloadedP2PSegment
import com.npaw.p2p_manager.utils.StringUtil
import com.npaw.utils.Log
import java.io.IOException

class ExoPlayerPeerDataSource(private val dataManager: DataManager,
                              private val upstreamDataSource: DataSource) : BaseDataSource(true) {

    // Exoplayer DataSource Logic
    private lateinit var dataSpec: DataSpec
    private lateinit var dataSpecKey: String
    private var dataSink: ByteArrayDataSink? = null

    // P2p DataSource Logic
    private var useP2P = false
    private var manifest = false
    private var connectedPeerId: String? = null
    private val identifier: DataSourceId = DataManager.assignId()
    private var totalRead: Long = 0

    // Statistics
    private var downloadStart: Long = 0
    private var downloadTime: Long = 0

    override fun getUri(): Uri {
        return dataSpec.uri
    }

    @Throws(IOException::class)
    override fun open(dataSpecInput: DataSpec): Long {
        dataSpec = dataSpecInput
        connectedPeerId = null
        downloadTime = 0
        totalRead = 0
        manifest = false
        val cleanUrl = StringUtil.getUrlPath(dataSpec.uri.toString())
        val range: String = buildRangeRequestHeader(dataSpec.position, dataSpec.length)
        dataSpecKey = StringUtil.generateHash(cleanUrl + range) ?: "0"

        Log.debug("DataSource: Opening dataSpec - " + dataSpec.uri.toString() + " - key: $dataSpecKey")

        val peerManager: PeerManager? =
            if (cleanUrl.endsWith(".mpd") || cleanUrl.endsWith(".m3u8")) {
                manifest = true
                null
            } else {
                manifest = false
                dataManager.hasSegment(dataSpecKey)
            }

        var bytesRemaining = 0L

        peerManager?.let { peer ->

            bytesRemaining = useP2p(peer)

            if (bytesRemaining == 0L ) {
                bytesRemaining = useHttp()
            } else {
                connectedPeerId = peer.getPeerId()
                Log.debug("P2p: Opening dataSpec in P2P expecting $bytesRemaining for Key: $dataSpecKey")
            }

        } ?: run {
            bytesRemaining = useHttp()
        }

        dataSink = ByteArrayDataSink()
        dataSink?.open(dataSpec)
        downloadStart = System.currentTimeMillis()

        return bytesRemaining
    }

    @Throws(IOException::class)
    override fun read(buffer: ByteArray, offset: Int, length: Int): Int {
        var readBytes = 0

        try {
            readBytes = if (useP2P) {
                dataManager.read(buffer, offset, length, identifier)
            } else {
                upstreamDataSource.read(buffer, offset, length)
            }
        } catch (e: IOException) {
            e.let { Log.error(it.stackTraceToString()) }
        }

        if (readBytes > 0) {
            dataSink?.write(buffer, offset, readBytes)
            totalRead += readBytes.toLong()
        }

        return readBytes
    }

    @Throws(IOException::class)
    override fun close() {

        downloadTime = System.currentTimeMillis() - downloadStart

        if (downloadTime > 0) {
            val bandwidthBps = (8 * totalRead / (downloadTime / 1000.0)).toLong()
            if (useP2P) registerDownloadedSegment(bandwidthBps, downloadTime)
        }

        Log.debug("DataSource: Closing dataSpec - " + dataSpec.uri.toString() + " - key: $dataSpecKey")

        dataSink?.let {
            it.close()
            it.data?.let { data ->
                if (manifest) { dataManager.parseManifest(String(data)) }
                dataManager.cacheSegment(dataSpecKey, data)
                dataManager.sendKeys()
            }
        }

        if (useP2P) {
            dataManager.closeDownload(dataSpecKey)
        } else {
            upstreamDataSource.close()
        }
    }

    /**
     * Uses http to request a segment download.
     *
     * @return Size of data to be downloaded from [upstreamDataSource].
     */
    private fun useHttp(): Long {
        useP2P = false
        return try {
            val bytesRemaining = upstreamDataSource.open(dataSpec)
            if (dataSpec.length.toInt() == C.LENGTH_UNSET && bytesRemaining.toInt() != C.LENGTH_UNSET) {
                dataSpec = dataSpec.subrange(0, bytesRemaining)
            }
            bytesRemaining
        } catch (e: IOException) {
            e.let { Log.error(it.stackTraceToString()) }
            upstreamDataSource.close()
            0L
        }
    }

    /**
     * Uses p2p to request a segment download.
     *
     * Blocks the application flow until peer response segment request or timeout is exceeded.
     *
     * @param peer The [PeerManager] that has de segment.
     * @return Size of data to be downloaded from [PeerManager] or **Long = 0L** if timeout is exceeded.
     */
    private fun useP2p(peer: PeerManager): Long {
        useP2P = true
        return dataManager.requestSegment(peer, dataSpecKey, identifier)
    }

    private fun registerDownloadedSegment(bandwidthBps: Long, downloadTime: Long) {
        dataManager.registerDownloadedSegment(
            DownloadedP2PSegment(
                fromPeerId = connectedPeerId,
                bytes = totalRead,
                bandwidth = bandwidthBps,
                downloadMillis = downloadTime
            )
        )
    }

    fun destroy() {
        // TODO how to call it?
        dataManager.removeDataSource(identifier)
    }

    private fun buildRangeRequestHeader(position: Long, length: Long): String {
        if (position == 0L && length == C.LENGTH_UNSET.toLong()) {
            return ""
        }
        val rangeValue = StringBuilder()
        rangeValue.append("bytes=")
        rangeValue.append(position)
        rangeValue.append("-")
        if (length != C.LENGTH_UNSET.toLong()) {
            rangeValue.append(position + length - 1)
        }
        return rangeValue.toString()
    }
}
