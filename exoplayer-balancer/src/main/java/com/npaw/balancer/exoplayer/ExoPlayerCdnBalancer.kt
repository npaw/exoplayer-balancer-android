package com.npaw.balancer.exoplayer

import android.content.Context

import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSource
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSource

import com.npaw.balancer.CdnBalancer
import com.npaw.balancer.exoplayer.BuildConfig.VERSION_NAME
import com.npaw.balancer.utils.BalancerOptions

import okhttp3.OkHttpClient

class ExoPlayerCdnBalancer @JvmOverloads constructor(
    accountCode: String,
    context: Context,
    options: BalancerOptions = BalancerOptions()
) : CdnBalancer(accountCode, context, options) {

    @JvmOverloads
    fun getMediaSourceFactory(settings : MediaSourceSettings? = null, customOkHttpInterceptor: OkHttpDataSource.Factory? = null): MediaSource.Factory {

        val okHttpDataSource = customOkHttpInterceptor ?: OkHttpDataSource.Factory(
            OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
        )

        val mediaSourceFactory = DefaultMediaSourceFactory {
            ExoPlayerPeerDataSource(engine.p2pLoader.dataManager, okHttpDataSource.createDataSource())
        }.apply {
            settings?.let { setting ->

                setting.adsLoaderProvider?.let {
                    if (setting.adViewProvider != null) setLocalAdInsertionComponents(it, setting.adViewProvider)
                } ?: setting.adsLoaderProviderDeprecated?.let {
                    setAdsLoaderProvider(setting.adsLoaderProviderDeprecated)
                    setAdViewProvider(setting.adViewProvider)
                }

                setting.loadErrorHandlingPolicy?.let { setLoadErrorHandlingPolicy(it) }
                setting.drmSessionManagerProvider?.let { setDrmSessionManagerProvider(it) }
                setting.liveMaxOffsetMs?.let { setLiveMaxOffsetMs(setting.liveMaxOffsetMs) }
                setting.liveMinOffsetMs?.let { setLiveMinOffsetMs(setting.liveMinOffsetMs) }
                setting.liveMaxSpeed?.let { setLiveMaxSpeed(setting.liveMaxSpeed) }
                setting.liveMinSpeed?.let { setLiveMinSpeed(setting.liveMinSpeed) }
                setting.liveTargetOffsetMs?.let { setLiveTargetOffsetMs(setting.liveTargetOffsetMs) }
                setting.useProgressiveMediaSourceForSubtitles?.let { experimentalUseProgressiveMediaSourceForSubtitles(setting.useProgressiveMediaSourceForSubtitles) }
                setting.serverSideAdInsertionMediaSourceFactory?.let { setServerSideAdInsertionMediaSourceFactory(setting.serverSideAdInsertionMediaSourceFactory) }
            }
        }

        return mediaSourceFactory
    }

    override fun getVersion(): String { return VERSION_NAME + "-" + getAdapterName() }
    override fun getAdapterName(): String { return "ExoPlayerBalancer-2.17" }
}